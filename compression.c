#include <stdlib.h>
#include <string.h>

#include "configuration.h"
#include "reference_tree.h"

#include "compression.h"

static process_return_code_t write_archive(processing_request_t *request);

process_return_code_t compress(processing_request_t *request)
{
    process_return_code_t return_code;
    reference_tree_t reference_tree = {0};

    return_code = build_reference_tree(request, &reference_tree);
    if (return_code == OK)
    {
        if (request->verbose)
        {
            print_reference_tree(&reference_tree);
        }
    }

    if (return_code == OK)
    {
        return_code = write_archive(request);
    }

    if (request->output_path)
    {
        fclose(request->output_file);
    }

    return return_code;
}

static process_return_code_t write_archive(processing_request_t *request)
{
    return OK;
}
