#include <ctype.h>
#include <stdlib.h>

#include "common.h"
#include "priority_queue.h"
#include "reference_tree.h"

#define ONE_MEGABYTE (1048576)

static process_return_code_t count_references(processing_request_t *request, reference_tree_t *reference_tree);

process_return_code_t build_reference_tree(processing_request_t *request, reference_tree_t *reference_tree)
{
    process_return_code_t return_code;
    priority_queue_t queue = {0, NULL};

    reference_tree->elements = calloc(PROCESSING_UNIT_VALUE_COUNT, sizeof(reference_tree_element_t));
    if (reference_tree->elements == NULL)
    {
        return MEMORY_ERROR;
    }

    return_code = count_references(request, reference_tree);

    if (return_code == OK)
    {
        for (int i = 0; i < PROCESSING_UNIT_VALUE_COUNT && return_code == OK; ++i)
        {
            reference_tree_element_t *element = &reference_tree->elements[i];

            element->sequence = i;

            if (element->reference_count > 0)
            {
                return_code = priority_queue_push(&queue, element);
            }
        }
    }

    if (return_code == OK && request->verbose)
    {
        priority_queue_print(&queue);
    }

    if (return_code == OK)
    {
        reference_tree_element_t *first, *second, *internal;

        while (queue.length > 1 && return_code == OK)
        {
            first = priority_queue_pop(&queue);
            second = priority_queue_pop(&queue);

            internal = calloc(1, sizeof(reference_tree_element_t));

            internal->reference_count = first->reference_count + second->reference_count;
            internal->left = first;
            internal->right = second;

            first->parent = internal;
            second->parent = internal;

            return_code = priority_queue_push(&queue, internal);
        }
    }

    return return_code;
}

static process_return_code_t count_references(processing_request_t *request, reference_tree_t *reference_tree)
{
    processing_unit_t *buffer;
    uint64_t read_bytes, all_bytes = 0, file_size;

    buffer = calloc(PROCESSING_UNIT_BYTES, request->reference_counting_buffer_size);
    if (buffer == NULL)
    {
        return MEMORY_ERROR;
    }

    if (request->progress)
    {
        puts("Reference counting");

        fseek(request->input_file, 0, SEEK_END);
        file_size = ftell(request->input_file);
        fseek(request->input_file, 0, SEEK_SET);
    }

    while ((read_bytes =
                fread(buffer, PROCESSING_UNIT_BYTES, request->reference_counting_buffer_size, request->input_file)) > 0)
    {
        for (uint32_t i = 0; i < read_bytes && i < request->reference_counting_buffer_size; ++i, all_bytes++)
        {
            if (request->progress && (all_bytes % (ONE_MEGABYTE / 10) == 0))
            {
                printf("\r%02.2f %%  %ld of %ld bytes", (float)all_bytes / file_size * 100.0, all_bytes, file_size);
                fflush(stdout);
            }

            reference_tree->elements[buffer[i]].reference_count++;
        }
    }

    if (request->progress)
    {
        putchar('\n');
    }

    return OK;
}

void print_reference_tree(reference_tree_t *reference_tree)
{
    char text[PROCESSING_UNIT_BITS + 1];
    char *ptr;

    printf("%10s %10s %10s %10s %10s\n", "ID", "SEQUENCE", "CHARACTER", "REF CNT", "CODE");
    for (int i = 0; i < PROCESSING_UNIT_VALUE_COUNT; ++i)
    {
        reference_tree_element_t *element = &reference_tree->elements[i];

        if (element->reference_count > 0)
        {
            ptr = &text[PROCESSING_UNIT_BITS];
            *ptr = '\0';

            --ptr;

            while (element->parent)
            {
                if (element->parent->left == element)
                {
                    *ptr = '0';
                }
                else if (element->parent->right == element)
                {
                    *ptr = '1';
                }
                else
                {
                    *ptr = 'E';
                }

                --ptr;
                element = element->parent;
            }

            element = &reference_tree->elements[i];
            printf("%10d %10x %10c %10ld %10s\n", i, element->sequence,
                   isprint(element->sequence) ? element->sequence : ' ', element->reference_count, ptr + 1);
        }
    }
}
