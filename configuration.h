#ifndef __CONFIGURATION_H__
#define __CONFIGURATION_H__

#include <stdbool.h>

#include "common.h"
#include "reference_tree.h"

process_return_code_t process_command_line_arguments(int argc, char **argv, processing_request_t *request);

#endif
