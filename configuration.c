#include <string.h>
#include <unistd.h>

#include "common.h"
#include "configuration.h"

#define ONE_GIGABYTE (1073741824)
#define DEFAULT_REFERENCE_COUNTING_BUFFER_SIZE (1048576)

process_return_code_t process_command_line_arguments(int argc, char **argv, processing_request_t *request)
{
    int opt;
    int tmp;
    process_return_code_t return_code = OK;

    memset(request, 0, sizeof(processing_request_t));

    request->reference_counting_buffer_size = DEFAULT_REFERENCE_COUNTING_BUFFER_SIZE;

    while ((opt = getopt(argc, argv, ":cdai:o:vw:r:l:hp")) != -1 && return_code == OK)
    {
        switch (opt)
        {
        case 'h':
            return_code = HELP_REQUESTED;
            break;
        case 'c':
            request->mode = COMPRESSION_MODE;
            break;
        case 'd':
            request->mode = DECOMPRESSION_MODE;
            break;
        case 'a':
            request->mode = ANALYSIS_MODE;
            break;
        case 'i':
            request->input_path = optarg;
            break;
        case 'o':
            request->output_path = optarg;
            break;
        case 'v':
            request->verbose = true;
            break;
        case 'p':
            request->progress = true;
            break;
        case 'r':
            if (sscanf(optarg, "%d", &tmp) != 1)
            {
                request->reference_counting_buffer_size = DEFAULT_REFERENCE_COUNTING_BUFFER_SIZE;
            }
            else
            {
                if (tmp < 0 || tmp > ONE_GIGABYTE)
                {
                    return_code = WRONG_PARAMETER;
                }

                request->reference_counting_buffer_size = tmp;
            }
            break;
        case ':':
            printf("Option %c needs a value\n", opt);
            return_code = WRONG_PARAMETER;
        case '?':
            printf("Unknown option: %c\n", optopt);
            return_code = WRONG_PARAMETER;
        }
    }

    if (return_code != HELP_REQUESTED && request->mode == UNKNOWN_MODE)
    {
        return_code = WRONG_PARAMETER;
    }

    if (request->input_path == NULL)
    {
        request->input_file = stdin;
    }

    if (request->output_path == NULL)
    {
        request->output_file = stdout;
    }

    if (return_code != OK)
    {
        printf("%s parameters:\n"
               "    -c - Compression mode\n"
               "    -d - Decompression mode\n"
               "    -a - Analysis mode\n"
               "    -i - Input file path\n"
               "    -o - Ouput file path\n"
               "    -r - Reference counting window size\n"
               "    -v - Verbose mode\n"
               "    -p - Print progress bar\n"
               "    -h - Help menu\n",
               argv[0]);
    }

    return return_code;
}
