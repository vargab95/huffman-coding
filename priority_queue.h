#ifndef __PRIORITY_QUEUE_H__
#define __PRIORITY_QUEUE_H__

#include "common.h"
#include "reference_tree.h"

typedef struct priority_queue_element_t
{
    reference_tree_element_t *element;
    struct priority_queue_element_t *next;
} priority_queue_element_t;

typedef struct 
{
    uint32_t length;
    priority_queue_element_t *head;
} priority_queue_t;

process_return_code_t priority_queue_push(priority_queue_t *queue, reference_tree_element_t *element);
reference_tree_element_t* priority_queue_pop(priority_queue_t *queue);
void priority_queue_print(priority_queue_t *queue);

#endif
