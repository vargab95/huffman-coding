#include <ctype.h>
#include <stdlib.h>

#include "common.h"
#include "priority_queue.h"

process_return_code_t priority_queue_push(priority_queue_t *queue, reference_tree_element_t *element)
{
    priority_queue_element_t *new, *previous, *current;

    new = malloc(sizeof(priority_queue_t));
    if (new == NULL)
    {
        return MEMORY_ERROR;
    }

    queue->length++;
    new->element = element;

    if (queue->head == NULL)
    {
        new->next = NULL;
        queue->head = new;

        return OK;
    }

    if (new->element->reference_count < queue->head->element->reference_count)
    {
        new->next = queue->head;
        queue->head = new;

        return OK;
    }

    previous = queue->head;
    current = previous->next;
    while (current && new->element->reference_count > current->element->reference_count)
    {
        previous = current;
        current = current->next;
    }

    previous->next = new;
    new->next = current;

    return OK;
}

reference_tree_element_t *priority_queue_pop(priority_queue_t *queue)
{
    priority_queue_element_t *first = queue->head;
    reference_tree_element_t *element = first->element;

    queue->head = first->next;
    queue->length--;

    free(first);

    return element;
}

void priority_queue_print(priority_queue_t *queue)
{
    priority_queue_element_t *current = queue->head;

    puts(" CODE CHAR   REF CNT");
    while (current)
    {
        printf("%5d%5c%10ld\n", current->element->sequence,
               isprint(current->element->sequence) ? current->element->sequence : ' ',
               current->element->reference_count);
        current = current->next;
    }

    putchar('\n');
}
