#ifndef __REFERENCE_TREE_H__
#define __REFERENCE_TREE_H__

#include "common.h"
#include <stdbool.h>
#include <stdint.h>

typedef struct reference_tree_element_t
{
    uint64_t reference_count;
    processing_unit_t sequence;
    processing_unit_t bit_pattern;
    struct reference_tree_element_t *parent;
    struct reference_tree_element_t *left;
    struct reference_tree_element_t *right;
} reference_tree_element_t;

typedef struct
{
    reference_tree_element_t *elements;
} reference_tree_t;

process_return_code_t build_reference_tree(processing_request_t* request, reference_tree_t *reference_tree);
void print_reference_tree(reference_tree_t *reference_tree);
void print_reference_tree_element(reference_tree_element_t *element, uint8_t indent);

#endif
